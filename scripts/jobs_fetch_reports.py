"""
This script fetches and merges the pytest integration
JUnit reports of a Gitlab CI pipeline. The resulting merged
JUnit report can be used as input to the pytest job selection
plugin, used for job balancing.
"""
import re
import argparse
import urllib.parse
import urllib.request
import urllib.error
import json
import io
from typing import List, TypedDict, Tuple, Optional
import xml.etree.ElementTree as ET
import sys
import os

ProjectPipelineJob = TypedDict('ProjectPipelineJob', {'id': int, 'name': str})


def gitlab_api_project_pipeline_jobs(
    project_id: str,
    pipeline_id: int,
) -> List[ProjectPipelineJob]:
    """
    Fetch ProjectPipelineJobs of pipeline `pipeline_id` in project `project_id`
    """
    project_id_quoted = urllib.parse.quote_plus(str(project_id))

    per_page = 200
    endpoint = 'https://gitlab.com/api/v4/'
    path = f'projects/{project_id_quoted}/pipelines/{pipeline_id}/jobs'
    query = f'?per_page={per_page}'
    next_url: Optional[str] = endpoint + path + query

    # Use link header to detect pagination as per
    # https://docs.gitlab.com/ee/api/#offset-based-pagination
    next_link_re = re.compile(r'<([^>]*)>; rel="next"')
    jobs: List[ProjectPipelineJob] = []
    try:
        while next_url:
            with urllib.request.urlopen(next_url) as request:
                data = json.loads(request.read().decode())
                jobs = jobs + [
                    {'id': job['id'], 'name': job['name']} for job in data
                ]

                link = request.headers.get('link', '')
                next_link_match = next_link_re.search(link)
                if next_link_match:
                    next_url = next_link_match.group(1)
                else:
                    print("No next link, terminating")
                    next_url = None

        return jobs

    except urllib.error.HTTPError as exc:
        print(
            "Could not fetch jobs of pipeline "
            + f"{pipeline_id} in {project_id}: {exc}"
        )
        sys.exit(1)


def gitlab_api_project_pipeline_job_artifact(
    project_id: str, job_id: int, artifact_path: str
) -> bytes:
    """
    Fetch artifact at `artifact_path` of job `job_id` in `project_id`
    """
    project_id_quoted = urllib.parse.quote_plus(str(project_id))
    artifact_path_quoted = "/".join(
        map(urllib.parse.quote_plus, artifact_path.split("/"))
    )

    endpoint = 'https://gitlab.com/api/v4/'
    path = (
        f'projects/{project_id_quoted}/jobs/{job_id}'
        + f'/artifacts/{artifact_path_quoted}'
    )
    url = endpoint + path

    try:
        with urllib.request.urlopen(url) as request:
            return request.read()
    except urllib.error.HTTPError as exc:
        print(
            f"Could not fetch artifact {artifact_path} "
            + f"in job {job_id} in {project_id}: {exc}"
        )
        sys.exit(1)


def merge_junit_reports(
    junit_reports: List[ET.ElementTree],
) -> Tuple[int, ET.ElementTree]:
    """
    Merges the testsuites of a list of JUnit XML reports
    """

    testcases: List[ET.Element] = []
    for junit_report in junit_reports:
        testsuite = junit_report.getroot().find('testsuite')
        assert isinstance(testsuite, ET.Element)
        testcases += testsuite.findall('testcase')

    nb_testcases = len(testcases)
    testsuites = ET.Element('testsuites')
    testsuite = ET.SubElement(testsuites, 'testsuite')
    testsuite.set('tests', str(nb_testcases))
    testsuite.extend(testcases)
    return (nb_testcases, ET.ElementTree(testsuites))


def fetch_and_merge_reports(
        project_id: str,
        pipeline_id: int,
        job_regexp: re.Pattern,
        junit_artifact_path: str,
        merged_report_output_file: io.TextIOWrapper,
) -> None:
    """
    Fetches and merges the JUnit XML reports of each pytest
    integration job in pipeline `pipeline_id` of project `project_id`,
    and writes the result to `merged_report_output_file`.
    """

    jobs = gitlab_api_project_pipeline_jobs(project_id, pipeline_id)
    junit_reports = []
    for job in jobs:
        match = job_regexp.match(job['name'])
        if match is None:
            continue

        artifact_path = re.sub(job_regexp, junit_artifact_path, job['name'])
        junit_report_str = gitlab_api_project_pipeline_job_artifact(
            project_id, job['id'], artifact_path
        ).decode(encoding="utf-8")

        print(f"Job {job['id']} ({job['name']}): "
              + f"Fetched {artifact_path}")
        junit_reports.append(ET.ElementTree(
            ET.fromstring(junit_report_str)
        ))

    if len(junit_reports) == 0:
        print(
            f"Found no jobs in pipeline {pipeline_id} of project "
            + f"{project_id} matching /{job_regexp.pattern}/"
        )
        sys.exit(1)

    (nb_testcases, merged_junit_report) = merge_junit_reports(junit_reports)
    ET.indent(merged_junit_report)
    merged_junit_report.write(merged_report_output_file, encoding='unicode')
    print(
        f"Wrote merged report with {nb_testcases} testcases "
        + f"to {merged_report_output_file.name}"
    )


def main() -> None:
    prog = os.path.basename(sys.argv[0])
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=f"""
Fetches a individual JUnit XML reports from the artifacts of a
subset of jobs in a GitLab CI pipelines and merges them.

SELECTING JOBS AND ARTIFACTS

This script will download a single artifact from all jobs in the specified
pipeline whose job name matches the regular expression in the positional
argument 'job_regexp'. The artifact downloaded is decided by the third
positional argument 'junit_artifact_path'. These two will be
passed as the first respectively second argument to Python's 're.sub'
function and so should match the formats expected by this function.

In short: given a job named 'job_name', if the 'job_name' matches 'job_regexp',
then this script will download and merge:

  re.sub(job_regexp, junit_artifact_path, job_name)

EXAMPLES

To download and merge the artifacts at the path 'reports/junit.xml' in jobs
named 'pytest' in the project 'foo/bar' of pipeline #1234 into the local
file 'junit.xml', call:

  {prog} foo/bar 1234 pytest 'reports/junit.xml' 'junit.xml'

Suppose a pipeline #1234 in project 'foo/bar':
 - which contains the jobs: 'build', 'pytest 1/2', and 'pytest 2/2',
 - where the job 'build' writes no JUnit XML report, and
 - where the jobs 'pytest X/Y' writes JUnit XML reports at the path
   'reports/junit_X_Y.xml'.

To download and merge the JUnit XML reports of this pipeline into the
local file 'junit.xml', call:

  {prog} foo/bar 1234 'pytest (\\d+)/(\\d+)' 'reports/junit_\\1_\\2.xml' \
         'junit.xml'

ARGUMENTS

""")

    parser.add_argument(
        "project_id",
        type=str,
        help="""
        Provide gitlab project id as first argument, e.g.
        `tezos/tezos` or numerical id
        """,
    )

    parser.add_argument(
        "pipeline_id",
        type=int,
        help="""
        Provide pipeline id as second argument, e.g. 391452179
        """,
    )

    parser.add_argument(
        "job_regexp",
        type=re.compile,
        help="""
        A regexp matching GitLab CI job names from which to fetch reports.
        See 'SELECTING JOBS AND ARTIFACTS' above for more information.

        Example: 'pytest (\\d+)/(\\d+)'
        """,
    )

    parser.add_argument(
        "junit_artifact_path",
        type=str,
        help="""
        The path in a jobs artifacts containing the report to download.
        See 'SELECTING JOBS AND ARTIFACTS' above for more information.

        Example: 'reports/report_\1_\2.xml'
        """,
    )

    parser.add_argument(
        "merged_report_output_file",
        type=argparse.FileType('w'),
        help="""
        Where to write merged report
        """,
    )

    args = parser.parse_args()

    fetch_and_merge_reports(
        args.project_id,
        args.pipeline_id,
        args.job_regexp,
        args.junit_artifact_path,
        args.merged_report_output_file
    )


if __name__ == "__main__":
    main()
