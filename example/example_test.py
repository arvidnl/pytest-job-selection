import time
import os

def sleep(period: float):
    if not os.getenv("QUICK"):
        time.sleep(period)

class TestExampleA:
    def test_a(self):
        sleep(1)

class TestExampleB:
    def test_b(self):
        sleep(2)

class TestExampleC:
    def test_c(self):
        sleep(1)

class TestExampleD:
    def test_d(self):
        sleep(2)
